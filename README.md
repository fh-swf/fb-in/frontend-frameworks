# Frontend-Frameworks

Materialien zum Modul Frontend-Frameworks für Webanwendungen

## Praktikum 1: Bierdatenbank mit Vue.js

![](Screenshot_Praktikum1.png)

In der Klausur zum Modul Internettechnologien haben wir eine Bierdatenbank dargestellt.
Nun wollen wir die Aufgabe mit Vue.js umsetzen.

Dazu gibt es im Verzeichnis `praktikum_01` eine Datei `beer.html`, die bereits eine Funktion zum Abruf der Bierdatenbank und zur Berechnung der
Farbe des Biers enthält.

### Aufgabe 1: Laden der Bierdaten

Eweitern Sie das _Viewmodel_ so, dass beim Laden der Seite die Bierdaten über das API geladen werden.
Dazu können Sie die Funktion `getBeerData()` in der Datei `beer.js` in einem geeigneten _Lifecycle-Hook_ verwenden.

### Aufgabe 2: Darstellung der Biere

Erzeugen Sie je Bier ein `article`-Element, das die Daten des Biers enthält:

- Eine Überschrift `h2` mit dem Namen des Biers.
- Eine `section` mit der _Tagline_ des Biers.
  Verwenden Sie dabei die CSS-Klasse `tagline`.
- Eine `section` mit der _Beschreibung_ des Biers.
  Verwenden Sie dabei die CSS-Klasse `description`.
- Ein `img`-Element mit dem Bild des Biers.

Die beigefügte CSS-Datei `beer.css` sollte für eine Darstellung der Seite wie im Screenshot oben sorgen.

### Aufgabe 3: Blättern in der Datenbank

Ergänzen Sie über der Liste der Biere Steuerelemente, die das Blättern in der Datenbank ermöglichen.

### Aufgabe 4: Popup anzeigen

Fügen Sie ein Popup-Fenster zur Seite hinzu, das bei einem Klick auf das den Artikel zu einem Bier angezeigt wird.

Das Popup-Element erhält
- eine Überschrift (`h1`) mit dem Namen des Bieres,
- eine `section` mit der Überschrift (`h2`) Description und der  
  Beschreibung (`beer.description`) des Bieres,
- eine `section` mit der Überschrift (`h2`) Food Pairings und einer 
  Liste zum Bier passender Gerichte (`beer.food_pairing`),
- eine `section` mit der Überschrift (`h2`) Alcohol und dem    
  Alkoholgehalt (`beer.abv`) des Bieres.
